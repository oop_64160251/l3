import java.util.Scanner;
public class Problem9For {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.print("Please input n: ");
        n = sc.nextInt();
        for(int i=0;i<n;i++){
            for(int k=0;k<n;k++){
                System.out.print(k+1);
            }
            System.out.println();
        }
        sc.close();
    }
    
}
