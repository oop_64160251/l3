import java.util.Scanner;
public class Problem9While {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.print("Please input n: ");
        n = sc.nextInt();
        int i=0;
        while(i<n){
            int k=0;
            while(k<n){
                System.out.print(k+1);
                k++;
            }
            System.out.println();
            i++;
        }
        sc.close();
    }
    
}
